from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.styles import PatternFill

sum_td = dict()   #  Teacher => sum(Days)
overview = dict() # Teacher => Dict(course => days)
periods = dict()  # Course=> Period
contacts = dict()  #  ? => Teacher

PD_TEAM = ['PMAD', 'STHA', 'FROS', 'LTPE']
GF_TEAM = ['HENK', 'LIME', 'JUDR', 'HKJJ', "TOMG", "BAAJ", "JOHNNY"]
INF_TEAM = ['DALF', 'KIKJ', 'THEB', 'JKO', 'HJJE', 'THVT', 'RAFI']

TEAMS = {"GF": GF_TEAM, "INF": INF_TEAM, "PD": PD_TEAM}

total_sum = 0

def eval_time(input_text, base = 'dage'):
    # print(input)
    if input_text is None:
        return 0
    if isinstance(input_text, int):
        if base == 'uger':
            input_text = float(input_text) * 5
        return input_text
    if isinstance(input_text, float):
        if base == 'uger':
            input_text = float(input_text) * 5
        return input_text

    input_text = input_text.strip()

    input_text = input_text.replace(" dage", " dag")
    input_text = input_text.replace(" uger", " uge")

    if input_text.endswith('dag'):
        input_text = input_text.replace(" dag", "")
        return float(input_text)

    if input_text.endswith('uge'):
        input_text = input_text.replace(" uge", "")
        input_text = input_text.replace(",", ".")
        input_text = float(input_text) * 5
        return float(input_text)

    if base == 'uger':
        input_text = float(input_text) * 5
        return input_text
    else:
        return float(input_text)


def updateStat(h, t, c, d):
    t = t.upper()
    t = t.strip()
    if t not in overview:
        overview[t] = dict()
    overview[t][c] = d

    if t in sum_td:
        sum_td[t] += d
    else:
        sum_td[t] = d


def parseFile(f, sheets, old=False):
    global total_sum
    base = 'dage'

    wb = load_workbook(f)
    print([item for item in wb.sheetnames if item not in sheets])
    for h in sheets:
        ws = wb[h]
        print(ws.title)
        contact_tmp = ""

        row = 14
        course = 'B'
        days = 'C'
        teacher = 'D'
        period = 'Uge ' + str(ws['C5'].value) + "-" + str(ws['C6'].value)
        contact_tmp = ws['C8'].value

        print("Kurser:")

        sum_days = 0

        while row < 100:  # safety margin
            c = ws[course + str(row)].value
            if c is None:
                break
            if c == "Dage:":
                break
            c = c + "(" + h + ")"
            periods[c] = period

            if contact_tmp is not None:
                contact_tmp = contact_tmp.replace("Kontaktlærer: ", "")
                contact_tmp = contact_tmp.replace("Kontaktlærer: ", "")
            contacts[h] = contact_tmp

            d = eval_time(ws[days + str(row)].value, base)
            t = ws[teacher + str(row)].value
            if t is None:
                t = "Ikke sat"
            if d is None:
                d = 0
            t = t.replace(" og ", "/")
            t1 = ""
            t2 = ""
            t3 = ""
            if t.count("/") == 2:
                t1, t2, t3 = t.split("/")
                updateStat(h, t1, c, d / 3)
                updateStat(h, t2, c, d / 3)
                updateStat(h, t3, c, d / 3)
            elif t.count("/") == 1:
                t1, t2 = t.split("/")
                updateStat(h, t1, c, d / 2)
                updateStat(h, t2, c, d / 2)
            elif "+" in t:
                t1, t2 = t.split("+")
                updateStat(h, t1, c, d)
                updateStat(h, t2, c, d)
            else:
                updateStat(h, t, c, d)
            sum_days += d
            row += 1
#            print(t)
#            print(c)
#            print(d)

        print("Total sum:\t" + str(sum_days) + "\n")
        total_sum += sum_days

def sort_teacher(s):
    t, z = s
    for key, team in TEAMS.items():
        if t in team:
            return key + t
    return "0"

def sort_teacher_sum(s):
    t, z = s
    return z

def findTeam(tech):
    for team in TEAMS.values():
        if tech in team:
            return team
    return None

def findTeamName(tech):
    for key, team in TEAMS.items():
        if tech in team:
            return key
    return None

def writeResult(file_name):
    wb = Workbook()

    ws1 = wb.create_sheet("Skitse-overblik")
    wb.active = 1
    row = 2
    ws1["A1"] = "Lærer"
    ws1["B1"] = "Team"
    ws1["C1"] = "Dage i skema"

    for tech, s in sorted(sum_td.items(), key=sort_teacher):
        team = findTeam(tech)
        ws1["A" + str(row)] = tech
        ws1["B" + str(row)] = findTeamName(tech)
        ws1["C" + str(row)] = s

        if team is None:
            pattern = PatternFill(start_color="CCC7CE", end_color="FFC7CE", fill_type="solid")
        elif list(TEAMS.values()).index(team) % 2 == 0:
            pattern = PatternFill(start_color="DDC7BB", end_color="FFC7CE", fill_type="solid")
        else:
            pattern = PatternFill(start_color="CCC7CE", end_color="CCC7CE", fill_type="solid")

        ws1["A" + str(row)].fill = pattern
        ws1["B" + str(row)].fill = pattern
        ws1["C" + str(row)].fill = pattern
        row += 1

    area = "C1:C" + str(row-1)
    ws1["A" + str(row+1)] = "Sum total"
    ws1["C" + str(row+1)] = "=SUM(" + area + ")"

    ws1["A" + str(row+2)] = "Gennemsnit"
    ws1["C" + str(row+2)] = "=MIDDEL(" + area + ")"

    ws1 = wb.create_sheet("Skitse-detajler")
    wb.active = 1
    row = 1
    for tech, X in overview.items():
        ws1["A" + str(row)] = tech
        for c1, d1 in X.items():
            ws1["B" + str(row)] = c1
            ws1["C" + str(row)] = d1
            ws1["E" + str(row)] = periods[c1]
            row += 1
        ws1["D" + str(row)] = sum_td[tech]
        row += 1

    ws1.column_dimensions['B'].width = 60
    ws1.column_dimensions['E'].width = 40

    ws1["C" + str(row+1)] = "=sum(C1..C" + str(row) + ")"
    ws1["D" + str(row+1)] = "=sum(D1..D" + str(row) + ")"


    ws1 = wb.create_sheet("Kontaktlærer")
    wb.active = 1
    row = 1
    for hold, teacher in contacts.items():
        ws1["A" + str(row)] = hold
        ws1["B" + str(row)] = teacher
        row += 1

    wb.save(file_name)

