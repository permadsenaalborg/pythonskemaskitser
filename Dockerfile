FROM python:3.4-alpine
RUN mkdir code
WORKDIR /code
ADD ExcelLib.py /code
ADD ReadExcel_Demo.py /code
ADD requirements.txt /code
RUN pip install -r requirements.txt
CMD ["python", "ReadExcel_Demo.py"]
