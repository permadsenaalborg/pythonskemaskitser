import ExcelLib as excel

in_folder = 'Data/in/'
out_folder = 'Data/out/'

pdfile = 'PD.xlsx'
inffile = 'Infra.xlsx'
gf2file = 'GF2.xlsx'
miscfile = 'diverse.xlsx'

PD_sheets = ['H1PD', 'H2PD', 'H3PD', 'H4PD', 'H5PD', 'H6PD', 'H7PD']
Inf_sheets = ['h1dt080119', 'h1dt090119']
# Inf_sheets = ['h1dt080119', 'h1dt090119', 'h2dt080119', 'h2dt090119', 'h3dt080119', 'h4id080119', 'h5id090119 ', 'h5xid090119 ', 'h5id090219 ', 'h6id090119', 'h7id080119', 'h7id090119', 'H9sd080119', 'H9sd090119']

GF2_sheets = ['Gdt010119', 'Gdt010219', 'Gdt010319']
misc_sheets = ['DEP', 'Diverse']


excel.parseFile(in_folder + pdfile, PD_sheets)
excel.parseFile(in_folder + inffile, Inf_sheets)
#excel.parseFile(in_folder + gf2file, GF2_sheets)
#excel.parseFile(in_folder + miscfile, misc_sheets)

excel.writeResult(out_folder + "Skitse-overblik.xlsx")
